import React, {useContext} from 'react';
import WizardWrapper from './components/WizardWrapper';
import StepsSection from './components/stepsSection/StepsSection';
import GenresOrSub from './components/genresOrSub/GenresOrSub';
import Navigation from './components/navigation/Navigation';

import { GenreContext } from './components/_context/AppContext';

const SecondStep = (props) => {

    const [, , , , subgenres, , , newSubgenre] = useContext(GenreContext);
    
    return (
        <WizardWrapper>
            <StepsSection active={"two"} />
            <GenresOrSub genres={subgenres} addNew />
            <Navigation back previousLocation="/" nextLocation={newSubgenre ? "/thirdStep": "/forthStep"} pathName={props.location.pathname}/>
        </WizardWrapper>
    );
}

export default SecondStep;