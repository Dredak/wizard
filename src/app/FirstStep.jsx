import React, {useContext} from 'react';
import WizardWrapper from './components/WizardWrapper';
import StepsSection from './components/stepsSection/StepsSection';
import GenresOrSub from './components/genresOrSub/GenresOrSub';
import Navigation from './components/navigation/Navigation';

import { GenreContext } from './components/_context/AppContext';

const FirstStep = (props) => {

    const [genres, , ,subgenresSetter] = useContext(GenreContext);
    
    return (
        <WizardWrapper>
            <StepsSection active={"one"}  />
            <GenresOrSub genres={genres} tagName="genre"/>
            <Navigation nextLocation="/secondStep" click={subgenresSetter} pathName={props.location.pathname}/>
        </WizardWrapper>
    );
}

export default FirstStep;