import React, {useContext} from 'react';
import {GenreContext} from '../components/_context/AppContext';

import './book-info.scss';

const BookInfo = (props) => {

    const [, , , , , , , , , , , , , ,bookInfoSetter, bookTitle, author, isbn, publisher, date, pages, format, edition, language, bookDescription] = useContext(GenreContext);
 
    return (
        <div className="book-info">
            <label >
                Book title
                <input
                name="bookTitle"
                value={bookTitle}type="text" 
                className="book-info__input book-info__input--full-width" 
                placeholder="Book title" 
                onChange={bookInfoSetter}/>
            </label>

            <label>
                Author
                <select 
                name="author" 
                value={author} 
                className="book-info__input book-info__input--full-width" 
                onChange={bookInfoSetter}>
                    <option value="">Choose author</option>
                    <option value="author 1">author 1</option>  
                    <option value="author 2">author 2</option>
                </select>
            </label>

            <label>
                ISBN
                <input 
                name="isbn" 
                value={isbn} 
                type="number"
                className="book-info__input book-info__input--full-width" 
                placeholder="ISBN" 
                onChange={bookInfoSetter}/>
            </label>

            <label>
                Publisher
                 <select 
                 name="publisher" 
                 value={publisher} 
                 className="book-info__input book-info__input--full-width" 
                 onChange={bookInfoSetter}>
                    <option value="" disabled selected>Choose publisher</option>
                    <option value="publisher 1">Publisher 1</option>
                    <option value="publisher 2">Publisher 2</option>
                </select>
            </label>

            <label>
            Date Published
                <input 
                name="date" 
                value={date} 
                type="date" 
                className="book-info__input" 
                onChange={bookInfoSetter}/>
            </label>

            <label>
                Number of pages
                <input 
                name="pages" 
                value={pages} 
                type="number" 
                className="book-info__input" 
                placeholder="Number of pages" 
                onChange={bookInfoSetter}/>
            </label>

            <label>
                Format
                <select 
                name="format" 
                value={format} 
                className="book-info__input" 
                onChange={bookInfoSetter}>
                    <option value="" disabled selected>Choose format</option>
                    <option value="format 1">Format 1</option>
                    <option value="format 2">Format 2</option>
                </select>
            </label>

            <div className="book-info__edition">
                <label>
                    Edition
                    <input 
                    name="edition" 
                    value={edition} 
                    type="text" 
                    className="book-info__input book-info__input--margin-right"
                     placeholder="Edition" 
                     onChange={bookInfoSetter}/>
                </label>
                <label>
                    Edition language
                    <select 
                    name="language" 
                    value={language} 
                    className="book-info__input" 
                    onChange={bookInfoSetter}>
                    <option value="" disabled selected>Choose edition language</option>
                    <option value="language 1">Language 1</option>
                    <option value="language 2">Language 2</option>
                    </select>
                </label>
            </div>
            
            <label>
                Description
                <textarea 
                name="bookDescription" 
                value={bookDescription} 
                className="book-info__input book-info__description book-info__input--full-width "
                placeholder="Type the description" 
                onChange={bookInfoSetter}></textarea>
            </label>
        </div>
    );
}

export default BookInfo;