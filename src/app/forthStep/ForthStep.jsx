import React, {useContext} from 'react';
import WizardWrapper from '../components/WizardWrapper';
import StepsSection from '../components/stepsSection/StepsSection';
import BookInfo from './BookInfo';
import Navigation from '../components/navigation/Navigation';

import { GenreContext } from '../components/_context/AppContext';

const ForthStep = (props) => {

    const [, , , , , , , newSubgenre] = useContext(GenreContext);
    const [logCompleteBookAndClearState] = useContext(GenreContext).slice(-1);
    return (
        <WizardWrapper>
            <StepsSection active={"four"} includeForth/>
            <BookInfo />
            
            <Navigation 
            back 
            click={logCompleteBookAndClearState} 
            previousLocation={newSubgenre ? "/thirdStep" : "/secondStep"} 
            nextLocation="/fifthStep" 
            pathName={props.location.pathname}/>

        </WizardWrapper>
    );
}

export default ForthStep;