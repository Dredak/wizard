import React from 'react';
import WizardWrapper from './components/WizardWrapper';
import { Link } from 'react-router-dom';

import './fifth-step.scss';

const FifthStep = () => {
    return (
        <WizardWrapper noTitle>
            <div className="fifth-step">
                <i className="fas fa-check-circle fifth-step__checked"></i>
                <p className="fifth-step__text">Book added successfully</p>
                <Link to={"/"} className="fifth-step__btn">Add another book</Link>
            </div>
        </WizardWrapper>
    );
}

export default FifthStep;