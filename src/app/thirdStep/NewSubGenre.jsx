import React, {useContext} from 'react';
import {GenreContext} from '../components/_context/AppContext';

const NewSubgenre = (props) => {

    const [, , , , , , , , ,subGenreFormSetter, requiredSetter, name, description, required] = useContext(GenreContext);

        return (
            <div className="new-subgenre" >
                <input 
                type="text" 
                name="name" 
                placeholder="subgenre name" 
                className="new-subgenre__name" 
                value={name}
                onChange={subGenreFormSetter} />

                <textarea 
                name="description" 
                placeholder="type the description..." 
                className="new-subgenre__description" 
                disabled= {required ? false : true } 
                value={description} 
                onChange={subGenreFormSetter}></textarea>

                <label className="new-subgenre__label" >
                    <input 
                    type="checkbox" 
                    className="new-subgenre__required" 
                    name="required" 
                    checked={required} 
                    onChange={requiredSetter} />
                    Description is required for this subgenre
            </label>
            </div>
        );
}

export default NewSubgenre;