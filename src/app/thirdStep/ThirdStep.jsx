import React from 'react';
import WizardWrapper from '../components/WizardWrapper';
import StepsSection from '../components/stepsSection/StepsSection';
import Navigation from '../components/navigation/Navigation';
import NewSubgenre from './NewSubGenre';


const ThirdStep = (props) => {


    return (
        <WizardWrapper>
            <StepsSection active={"three"} includeForth/>
            <NewSubgenre />
            <Navigation back previousLocation="/secondStep" nextLocation="/forthStep" pathName={props.location.pathname}/>
        </WizardWrapper>
    );
}

export default ThirdStep;