import React from 'react';
import FirstStep from './FirstStep';
import { AppContext } from './components/_context/AppContext';
import { Switch, Route } from 'react-router-dom';
import SecondStep from './SecondStep';
import ThirdStep from './thirdStep/ThirdStep';
import ForthStep from './forthStep/ForthStep';
import FifthStep from './FifthStep';

function App() {
  return (
    <AppContext>
      <Switch>
        <Route exact path="/" component={FirstStep} />
        <Route path="/secondStep" component={SecondStep} />
        <Route path="/thirdStep" component={ThirdStep} />
        <Route path="/forthStep" component={ForthStep} />
        <Route path="/fifthStep" component={FifthStep} />
      </Switch>
    </AppContext>
  );
}

export default App;
