import React from 'react';

import './components.scss';


const WizardWrapper = (props) => {

    return (
        <div className="wizard-wrapper">
            {!props.noTitle && <p>Add Book - New Book</p>}
            {props.children}
        </div>
    );
}

export default WizardWrapper;