import React from 'react';
import StepCircle from './StepCircle';
import StepLine from './StepLine';

const StepsSection = ({ includeForth, active }) => {

    return (
        <div className="steps-wrapper">
            <StepCircle stepNumber={1} enabled= {active === "one" ? "enabled" : ""} stepName={"Genre"} />
            <StepLine />
            <StepCircle stepNumber={2} enabled= {active === "two" ? "enabled" : ""} transformed stepName={"SubGenre"} />
            <StepLine />
            <StepCircle 
            stepNumber={includeForth ? 3 : "..."} 
            enabled= {active === "three" ? "enabled" : ""} 
            transformed 
            stepName={includeForth ? "Add new subgenre" : ""} />
            {
                includeForth &&
                <> <StepLine />
                    <StepCircle stepNumber={4} enabled= {active === "four" ? "enabled" : ""} transformed stepName={"Information"} />
                </>
            }

        </div>
    );
}

export default StepsSection;