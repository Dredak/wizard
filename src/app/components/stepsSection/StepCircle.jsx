import React from 'react';

const StepCircle = ({stepNumber, stepName, transformed, enabled}) => {
    return (
       <div className={`step-circle `}>
           <p className={`step-circle__circle ${enabled === "enabled" ? "step-circle__circle--enabled" : ""}`}>{stepNumber}</p>
           <p className={`step-circle__name ${transformed ? "step-circle__name--transformed" : ""}`}>{stepName}</p>
       </div>
    );
}

export default StepCircle;