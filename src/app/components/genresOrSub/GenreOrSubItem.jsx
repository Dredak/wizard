import React from 'react';

const GenreOrSubItem = ({ genre, children, clickedGenre, clickedGenreSetter, tagName, singleSubGenreSetter, singleSubGenre }) => {

    const element = genre ? genre.name : children;
    const  conditionActive = tagName === "genre" ? clickedGenre : singleSubGenre;

    return <p
        name={tagName}
        onClick={tagName === "genre" ?  clickedGenreSetter  : singleSubGenreSetter}
        className={`genre-or-sub__item ${conditionActive === element ? "active" : ""}`}  >
        {genre ? genre.name : children}
    </p>
}

export default GenreOrSubItem;