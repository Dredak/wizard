import React, { useContext } from 'react';
import GenreOrSubItem from './GenreOrSubItem';

import { GenreContext } from '../_context/AppContext';

const GenresOrSub = ({ genres, addNew, tagName }) => {

    const [, clickedGenre, clickedGenreSetter, , , singleSubGenre, singleSubGenreSetter, newSubgenre, newSubgenreSetter] = useContext(GenreContext);

    const genreOrSub = genres.map((genre) => {
        return <GenreOrSubItem
            genre={genre}
            key={genre.id}
            clickedGenreSetter={clickedGenreSetter}
            clickedGenre={clickedGenre}
            singleSubGenreSetter={singleSubGenreSetter}
            tagName={tagName}
            singleSubGenre={singleSubGenre} />
    })

    return (
        <div className="genre-or-sub">
            {genreOrSub}
            {addNew && <GenreOrSubItem
                clickedGenreSetter={clickedGenreSetter}
                clickedGenre={clickedGenre}
                singleSubGenreSetter={newSubgenreSetter}
                tagName={tagName}
                singleSubGenre={newSubgenre} key={999}>Add new</GenreOrSubItem>}
        </div>
    );
}

export default GenresOrSub;