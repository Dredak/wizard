import React, { createContext } from 'react';

const GenreContext = createContext();

class AppContext extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "genres": [
                {
                    "id": 1,
                    "name": "Genre 1",
                    "subgenres": [
                        {
                            "id": 10,
                            "name": "Subgenre 1",
                            "isDescriptionRequired": true,
                        }, {
                            "id": 11,
                            "name": "Subgenre 2",
                            "isDescriptionRequired": false
                        }, {
                            "id": 12,
                            "name": "Subgenre 3",
                            "isDescriptionRequired": true
                        }, {
                            "id": 13,
                            "name": "Subgenre 4",
                            "isDescriptionRequired": true
                        }, {
                            "id": 14,
                            "name": "Subgenre 5",
                            "isDescriptionRequired": true
                        }
                    ]
                }, {
                    "id": 2,
                    "name": "Genre 2",
                    "subgenres": [
                        {
                            "id": 15,
                            "name": "Subgenre 1",
                            "isDescriptionRequired": true
                        }, {
                            "id": 16,
                            "name": "Subgenre 2",
                            "isDescriptionRequired": false
                        }, {
                            "id": 17,
                            "name": "Subgenre 3",
                            "isDescriptionRequired": true
                        }
                    ]
                }, {
                    "id": 3,
                    "name": "Genre 3",
                    "subgenres": [
                        {
                            "id": 18,
                            "name": "Subgenre 1",
                            "isDescriptionRequired": true
                        }, {
                            "id": 19,
                            "name": "Subgenre 2",
                            "isDescriptionRequired": true
                        }, {
                            "id": 20,
                            "name": "Subgenre 3",
                            "isDescriptionRequired": true
                        }
                    ]
                }, {
                    "id": 4,
                    "name": "Genre 4",
                    "subgenres": [
                        {
                            "id": 21,
                            "name": "Subgenre 1",
                            "isDescriptionRequired": false
                        }, {
                            "id": 22,
                            "name": "Subgenre 2",
                            "isDescriptionRequired": false
                        }, {
                            "id": 23,
                            "name": "Subgenre 3",
                            "isDescriptionRequired": false
                        }
                    ]
                }, {
                    "id": 5,
                    "name": "Genre 5",
                    "subgenres": [
                        {
                            "id": 24,
                            "name": "Subgenre 1",
                            "isDescriptionRequired": true
                        }
                    ]
                }
            ],
            //first step data
            clickedGenre: "",
            subgenres: [],
            //second step data
            singleSubGenre: "",
            newSubgenre: "",
            //third step data
            name: "",
            description: "",
            required: false,
            //forth step data
            bookTitle: "",
            author: "",
            isbn: "",
            publisher: "",
            date: "",
            pages: "",
            format: "",
            edition: "",
            language: "",
            bookDescription: ""
        }
    }

    clickedGenreSetter = (e) => {
        const clickedElement = e.target.innerText;
        const clicked = this.state.clickedGenre;

        if (clicked !== clickedElement) {
            this.setState({ clickedGenre: clickedElement });
        } else {
            this.setState({ clickedGenre: "" });
        }
    }

    subgenresSetter = (e) => {
        const clicked = this.state.clickedGenre;
        const genres = this.state.genres;
        genres.map((genre) => {
            if (genre.name === clicked) {
                this.setState({ subgenres: genre.subgenres })
            }
        })
    }

    singleSubGenreSetter = (e) => {
        const clickedElement = e.target.innerText;
        const singleSubG = this.state.singleSubGenre;
        if (singleSubG !== clickedElement) {
            this.setState({ singleSubGenre: clickedElement, newSubgenre: "" });
        } else {
            this.setState({ singleSubGenre: "" });
        }
    }

    newSubgenreSetter = (e) => {
        const clickedElement = e.target.innerText;
        const newSubG = this.state.newSubgenre;
        if (newSubG !== clickedElement) {
            this.setState({ newSubgenre: clickedElement, singleSubGenre: "" });
        } else {
            this.setState({ newSubgenre: "" });
        }
    }

    subGenreFormSetter = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    requiredSetter = (e) => {
        if (e.target.name === "required") {
            this.setState((prevState) => {
                return { required: !prevState.required }
            })

        }
    }

    bookInfoSetter = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    logCompleteBookAndClearState = () => {
        const {
            clickedGenre,
            singleSubGenre,
            name,
            bookTitle,
            author,
            isbn,
            publisher,
            date,
            pages,
            format,
            edition,
            language,
            bookDescription } = this.state;

        console.log(`
            Genre: ${clickedGenre},
            Subgenre: ${singleSubGenre || name},
            Title: ${bookTitle},
            Author: ${author},
            Isbn: ${isbn},
            Publisher: ${publisher},
            Publishing date: ${date},
            Number of pages: ${pages},
            Format: ${format},
            Edition: ${edition},
            Language: ${language},
            Description: ${bookDescription}
            `)

        this.setState({
            clickedGenre: "",
            subgenres: [],
            newSubgenre: "",
            singleSubGenre: "",
            name: "",
            description: "",
            required: false,
            bookTitle: "",
            author: "",
            isbn: "",
            publisher: "",
            date: "",
            pages: "",
            format: "",
            edition: "",
            language: "",
            bookDescription: ""
        })
    }

    render() {

        const {
            genres,
            clickedGenre,
            subgenres,
            singleSubGenre,
            newSubgenre,
            name,
            description,
            required,
            bookTitle,
            author,
            isbn,
            publisher,
            date,
            pages,
            format,
            edition,
            language,
            bookDescription } = this.state;

        return (
            <GenreContext.Provider
                value={[
                    genres,
                    clickedGenre,
                    this.clickedGenreSetter,
                    this.subgenresSetter,
                    subgenres,
                    singleSubGenre,
                    this.singleSubGenreSetter,
                    newSubgenre,
                    this.newSubgenreSetter,
                    this.subGenreFormSetter,
                    this.requiredSetter,
                    name, description,
                    required,
                    this.bookInfoSetter,
                    bookTitle,
                    author,
                    isbn,
                    publisher,
                    date,
                    pages,
                    format,
                    edition,
                    language,
                    bookDescription,
                    this.logCompleteBookAndClearState
                ]}>
                {this.props.children}
            </GenreContext.Provider>
        );
    }
}


export {
    GenreContext,
    AppContext
}