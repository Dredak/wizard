import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import { GenreContext } from '../_context/AppContext';

const Navigation = ({ back, previousLocation, nextLocation, click, pathName }) => {

    const [, clickedGenre, , , , singleSubGenre, , newSubgenre, , , ,name, description, required, , bookTitle, author, isbn, publisher, date, pages, format, edition, language, bookDescription] = useContext(GenreContext);
  
    const thirdStepCondition = () => {
        if (!!name === true && !!required === false) {
            return true;
        } else if ( !!required === true){
            if (!!name === true && !!description === true){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    const forthStepCondition = () => {
        if (bookTitle && author && isbn && publisher && date && pages && format && edition && language && bookDescription){
            return true
        }
        return false;
    }

    const condition = pathName === "/" ? clickedGenre : pathName === "/secondStep" ? singleSubGenre || newSubgenre :  pathName === "/thirdStep" ? thirdStepCondition() : pathName === "/forthStep" ? forthStepCondition() : "";
    
    return (
        <div className="navigation">
            <div className="navigation__inner-wrapper">
                {back && <Link to={previousLocation} className="navigation__item-wrapper navigation__item-wrapper--hover">
                    <span>&lt;</span><p className="navigation__item-wrapper__back">Back</p>
                </Link>}
                <Link onClick={click} to={condition ? nextLocation : pathName} className={`navigation__item-wrapper ${condition ? "navigation__item-wrapper--hover" : "disabled"}`}>
                    <p className="navigation__item-wrapper__next">{pathName === "/forthStep" ? "Add" : "Next"}</p><span>&gt;</span>
                </Link>
            </div>
        </div>
    );
}

export default Navigation;